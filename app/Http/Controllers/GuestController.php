<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Newsletter;

use Illuminate\Support\Facades\Session;
//use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /*
     *   Guest Actions
     */
    public function newsletter(Request $request)
    {
        $input = $request->all();
        Newsletter::insert( array('email' => $input['email'], 'active' => 1) );
        return redirect()->back();
    }

    public function language()
    {
        if (Session::get('locale') != null && Session::get('locale') == "sk")
            $lang = "en";
        elseif (Session::get('locale') != null && Session::get('locale') == "en")
            $lang = "sk";
        else
            $lang = "en";
        session()->put('locale', $lang);

        return redirect()->back();
    }
}

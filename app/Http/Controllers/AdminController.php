<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        $menu = "dashboard";
        return view('admin.adminIndex', compact('menu', 'users'));
    }

    public function translate()
    {
        $en = include(base_path('resources/lang/en/content.php'));
        $sk = include(base_path('resources/lang/sk/content.php'));
        $menu = "translate";
        return view('admin.adminTranslate', compact('en', 'sk', 'menu'));
    }

    public function translateUpdate(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        $sk_data = "<?php return array(";
        $en_data = "<?php return array(";
        foreach($input as $key=>$value)
        {
            if (strpos($key,'en_') !== false) {
                $en_data .= "'".str_replace("en_", "", $key)."' => '".$value."',";
            }
            else
                $sk_data .= "'".str_replace("sk_", "", $key)."' => '".$value."',";

        }
        $sk_data .= " ); ?>";
        $en_data .= " ); ?>";

        $sk_file = fopen(base_path('resources/lang/sk/content.php'), "wb");
        fputs($sk_file, $sk_data);
        fclose($sk_file);

        $en_file = fopen(base_path('resources/lang/en/content.php'), "wb");
        fputs($en_file, $en_data);
        fclose($en_file);

        return redirect('admin/translate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

<!DOCTYPE html>
<html lang="en">
<!--
 _   _  _       _                        _
| | | |(_)     (_)                      | |
| | | | _  ____ _   ___   _ __      ___ | | __
| | | || ||_  /| | / _ \ | '_ \    / __|| |/ /
\ \_/ /| | / / | || (_) || | | | _ \__ \|   <
 \___/ |_|/___||_| \___/ |_| |_|(_)|___/|_|\_\
-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Aqua relax center</title>

    <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/ckeditor/adapters/jquery.js') }}"></script>

	<link href="{{ asset('/css/admin/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/admin/main.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- DataTables -->
	<link href='//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
	<link href='//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- SB Admin theme -->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">--}}

    <!-- Sidebar -->
    {{--<link rel="stylesheet" href="{{ asset('css/admin/simple-sidebar.css') }}">--}}

    <link rel="stylesheet" href="{{ asset('css/admin/dasper.css') }}">

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">Aqua relax center</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li @if(isset($menu) && $menu == "dashboard") class="active" @endif><a href="{{ url('admin') }}">Dashboard</a></li>
					<li @if(isset($menu) && $menu == "translate") class="active" @endif><a href="{{ url('admin/translate') }}">Translator</a></li>
				</ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
			</div>
		</div>
	</nav>

    <!--
    /*
     *  Content
     */
     -->
	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js"></script>

    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable( {
                responsive: true
            } );
        });
    </script>

    <!-- CKeditor (Wysiwyg text editor) -->
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'ckeditor' );
    </script>
</body>
</html>

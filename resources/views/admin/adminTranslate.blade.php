@extends('adminApp')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Translator</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/translate') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <p><strong>All inputs are required.</strong></p>
                                    <button type="submit" class="btn btn-primary col-md-12">
                                        Save
                                    </button>
                                </div>
                            </div>

                            @foreach( $en as $key=>$enValue)
                                <div class="form-group">
                                    <label class="col-md-2 control-label">{{ $key }}</label>
                                    <div class="col-md-5">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon" id="sizing-addon3">EN</span>
                                                <input type="text"
                                                       class="form-control"
                                                       name="en_{{ $key }}"
                                                       value="{{ $enValue }}"
                                                        @if( strpos($enValue, "<") )
                                                            title = "Don't delete <example></example> tags."
                                                        @elseif( strpos($enValue, ":parcel") )
                                                            title = "Don't delete :parcel tag."
                                                        @endif
                                                >
                                        </div>

                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon" id="sizing-addon3">SK</span>
                                            <input type="text"
                                                   class="form-control"
                                                   name="sk_{{ $key }}"
                                                   value="{{ $sk[$key] }}"
                                                    @if( strpos($enValue, "<") )
                                                        title = "Medzi znackami <example></example> sa nachadza zvyrazneny obsah, preto ich neodporucame odstranovat."
                                                    @elseif( strpos($enValue, ":parcel") )
                                                        title = "Znacka :parcel predstavuje cislo parcely, neodporucame odstranovat."
                                                    @endif
                                            >
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <p><strong>All inputs are required.</strong></p>
                                    <button type="submit" class="btn btn-primary col-md-12">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

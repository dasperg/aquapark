@extends('adminApp')

@section('content')

    <div class="left-panel col-md-4 col-md-offset-1">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-home" style="margin-right: 10px;"></i>
                    {{--<a href="{{ url('admin/apartment') }}">Apartments<span class="badge pull-right">{{ $apartments->count() }}</span></a>--}}
                </h3>
            </div>
            <div class="panel-body">

                <a href="{{ url('admin/apartment') }}" class="btn btn-default center-block">Apartment list</a>
            </div>

        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-comment" style="margin-right: 10px;"></i>
                    <a href="{{ url('admin/message') }}">
                        Messages
                        {{--<span class="badge pull-right">All {{ $messages->count() }}</span>--}}
                    </a>
                </h3>
            </div>
            <div class="panel-body">

                <a href="{{ url('admin/message') }}" class="btn btn-default center-block">Message list</a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-comment" style="margin-right: 10px;"></i>
{{--                    <a href="{{ url('admin/message') }}">Faq<span class="badge pull-right">{{ $faqs->count() }}</span></a>--}}
                </h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">

                </ul>
                <a href="{{ url('admin/faq') }}" class="btn btn-default center-block">Faq list</a>
            </div>
        </div>


    </div>

    <div class="right-panel col-md-4 col-md-offset-1">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-flag" style="margin-right: 10px;"></i>
                    {{--<a href="{{ url('admin/parcel') }}">Parcels<span class="badge pull-right">{{ $parcels->count() }}</span></a>--}}
                </h3>
            </div>
            <div class="panel-body">

                <a href="{{ url('admin/parcel') }}" class="btn btn-default center-block">Parcel list</a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-picture" style="margin-right: 10px;"></i>
                    <a href="{{ url('admin/newsletter') }}">Galleries</a>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                {{--<?php $i=1; ?>--}}
                    {{--@foreach( $galleries as $gallery)--}}
                        {{--<a href="{{ url('admin/gallery/'.$gallery->id) }}" class="thumbnail col-md-4" title="{{ $gallery->title_en }}">--}}
                            {{--<img src="{{ url($gallery->img) }}">--}}
                        {{--</a>--}}
                        {{--<?php $i++; ?>--}}
                        {{--@if( ($i%4)==0 ) <?php echo "</div><div class='row'>"; ?> @endif--}}
                    {{--@endforeach--}}
                </div>
                <a href="{{ url('admin/gallery') }}" class="btn btn-default center-block">Galleries</a>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-user" style="margin-right: 10px;"></i>
                    Users
                    <a href="{{ url('auth/register') }}" class="pull-right">Create user</a>
                </h3>

            </div>
            <div class="panel-body">
                <ul class="list-group">
                    @foreach( $users as $user)
                        <li class="list-group-item">
                            <div class="row">
                                <span class="col-md-5">{{ $user->name }}</span>
                                <span class="col-md-7">{{ $user->email }}</span>
                            </div>


                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-bullhorn" style="margin-right: 10px;"></i>
                    {{--<a href="{{ url('admin/newsletter') }}">Newsletters<span class="badge pull-right">{{ $newsletters->count() }}</span></a>--}}
                </h3>
            </div>
            <div class="panel-body">
                <ul class="list-group">
                    {{--@foreach( $newsletters->take(5) as $newsletter)--}}
                        {{--<li class="list-group-item">--}}
                            {{--<a href="mailto:{{ $newsletter->email }}">{{ $newsletter->email }}</a>--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                </ul>
                <a href="{{ url('admin/newsletter') }}" class="btn btn-default center-block">Newsletter list</a>
            </div>
        </div>
    </div>

@endsection
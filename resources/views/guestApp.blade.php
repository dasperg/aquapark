<!doctype html>
<html class="no-js" lang="sk">
<!--
 _   _  _       _                        _
| | | |(_)     (_)                      | |
| | | | _  ____ _   ___   _ __      ___ | | __
| | | || ||_  /| | / _ \ | '_ \    / __|| |/ /
\ \_/ /| | / / | || (_) || | | | _ \__ \|   <
 \___/ |_|/___||_| \___/ |_| |_|(_)|___/|_|\_\
-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Aqua relax center</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png') }}">
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:700,400,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    {{--<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.css') }}">--}}

    <!-- Opto graph data here  -->
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    {{--<script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>--}}
</head>

<body class="index">

<nav class="navbar" role="navigation">

</nav>

@yield('content')

<footer>

</footer>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
{{--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{ asset('js/vendor/jquery-2.1.4.min.js') }}}"><\/script>')</script>--}}

{{--<script src="{{ asset('js/main.js') }}"></script>--}}

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>
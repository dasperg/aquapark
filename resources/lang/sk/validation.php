<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "musíte <strong>súhlasiť so :attribute</strong>.",
	"active_url"           => ":attribute <strong>nie je</strong> platná cesta <strong>URL</strong>.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "The :attribute may only contain letters.",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The :attribute may only contain letters and numbers.",
	"array"                => "The :attribute must be an array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => [
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	],
	"boolean"              => "The :attribute field must be true or false.",
	"confirmed"            => "The :attribute confirmation does not match.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "pole <strong>:attribute</strong> musí mať <strong>správny formát</strong> E-mailovej adresy.",
	"filled"               => "pole :attribute <strong>musí byť vyplnené</strong>.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => ":attribute musí mať <strong>správny formát</strong>.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => [
		"numeric" => ":attribute nemôže byť <sttrong>väčšie ako :max</sttrong>.",
		"file"    => ":attribute nemôže byť <strong>väčší ako :max kB</strong>.",
		"string"  => ":attribute nemôže obsahovať <strong>viac ako :max znakov</strong>.",
		"array"   => ":attribute <strong>nemôže mať viac</strong> ako <strong>:max položky</strong>.",
	],
	"mimes"                => ":attribute musí obsahovať iba súbory nasledujúcich <strong>formátov: :values</strong>.",
	"min"                  => [
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	],
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "pole <strong>:attribute</strong> musí byť <strong>vyplnené</strong>.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => [
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "pole :attribute musí mať <strong>menej ako :size znakov</strong>.",
		"array"   => "The :attribute must contain :size items.",
	],
	"unique"               => "hodnota v poli <strong>:attribute</strong> je už <strong>použitá</strong>.",
	"url"                  => ":attribute nemá platný formát.",
	"timezone"             => "The :attribute must be a valid zone.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
        'permission' => [
            'denied' => 'Nemáte oprávenia na prístup.',
        ]
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
        'description' => 'popis',
        'name' => 'názov/meno',
        'photo' => 'obrázok',
        'img' => 'obrázok',
        'logo' => 'logo',
        'terms' => 'všeobecnými obchodnými podmienkami',
        'email' => 'e-mail',
        'password' => 'heslo',
        'password_confirmation' => 'potvrdenie hesla'
    ],

];

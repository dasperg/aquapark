<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Heslo musí mať aspoň 6 znakov a musí sa zhodovať s potvrdením.",
	"user" => "Nevieme nájsť užívateľa s touto e-mailovou adresou.",
	"token" => "Tento odkaz na reset hesla je už neplatný.",
	"sent" => "Na e-mail sme Vám poslali odkaz na reset hesla.",
	"reset" => "Nové heslo Vám bolo odoslané.",

];
